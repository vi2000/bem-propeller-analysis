%% BEM simulation of a rotor performance.

clc;
clear;
% Model Parameters
B = 4; %number of blades 
d = 1.184;%dry air density,25° C,sea level
u = 343*0.2; %m/s
omega = 1000/9.5492965964254; %1rad/s = 9.5492965964254rpm
 

geometry = load('r_chord_twist.mat');

r = geometry.chordtwistpropeller.r;
c = geometry.chordtwistpropeller.chord;
t = geometry.chordtwistpropeller.twist;
T_differential = zeros(length(r)-1,1);  
% for a specific U, omega

for i = 2:length(r)    
%a = induction_factor(u,omega,r(i),B,c(i),t(i));

lamda_r = omega*r(i)/u; % local tip-speed ratio
sigma_t = B*c(i)/(r(i)*2*pi); % local solidity
twist = deg2rad(t(i));
%Initialization

a_prev = 0.25*(2+pi*lamda_r*sigma_t-sqrt(4-4*pi*lamda_r*sigma_t+pi*lamda_r^2*sigma_t*(8*t(i)+pi*sigma_t))); %axial induction factor
a_t_prev =0; % tangential induction factor
phi_prev = atan2(u*(1-a_prev),(omega*r(i)*(1 + a_t_prev))); % advance angle

alpha = phi_prev - twist;

[cl,cd,~] = polars(alpha);

   
a = 1/(1+4*(sin(phi_prev))^2/(sigma_t*(cl*cos(phi_prev)+cd*sin(phi_prev))));
a_t = 1/(-1+4*sin(phi_prev)*cos(phi_prev)/(sigma_t*(cl*sin(phi_prev)-cd*cos(phi_prev))));
phi = atan2(u*(1-a),(omega*r(i)*(1 + a_t)));

while (abs(a_prev - a) > 1e-6 || abs(a_t_prev - a_t) > 1e-6 || abs(phi_prev-phi) >1e-6)
a_prev = a;
a_t_prev = a_t;
phi_prev = phi;

alpha = phi_prev - twist;
[cl,cd,~] = polars(alpha);
% cl = 0.8;
% cd = 0.01;
a = 1/(1+4*(sin(phi_prev))^2/(sigma_t*(cl*cos(phi_prev)+cd*sin(phi_prev))));
a_t = 1/(-1+4*sin(phi_prev)*cos(phi_prev)/(sigma_t*(cl*sin(phi_prev)-cd*cos(phi_prev))));
phi = atan(u*(1-a)/(omega*r(i)*(1 + a_t)));
end

a_end = a;
a_t_end = a_t;

T_differential(i-1) = 4*pi*r(i)*d*u^2*(1-a)*a*(r(i) - r(i-1));
end

T = sum(T_differential);



