function [a_end,a_t_end, alpha_end, phi_end] = induction_factor(u,omega,r,B,c,t,p)
%% To simulate run sim_plots.m

%% This function gives the axial induction factor - a_end and the tangential induction factor - a_t_end

% Input:
% u: descent velocity 
% omega: spin rate of the rotor 
% c: chord
% t: twist
% B - number of blades
% p: swashplate collective pitch angle in deg

%%
%lamda_r = omega*r/u; % local tip-speed ratio
%sigma_t = B*c/(r*2*pi); % local solidity
twist = deg2rad(t);
pitch = deg2rad(p);
%Initialization


a_prev = 0.1; %axial induction factor
a_t_prev =0.01; % tangential induction factor
phi_prev = atan(u*(1+a_prev)/(omega*r*(1 - a_t_prev))); % advance angle
alpha = (twist+pitch) - phi_prev;

[cl,cd,~] = polars(alpha,0.2);
epsilon = cd/cl;

a = B*cl*c*(cos(phi_prev)-epsilon*sin(phi_prev))/(8*r*pi*(sin(phi_prev))^2 +B*cl*c*epsilon*sin(phi_prev)-B*cl*c*cos(phi_prev));

a_t = B*cl*c*(epsilon*cot(phi_prev)+1)/(B*cl*c+8*pi*r*cos(phi_prev)+B*cl*c*epsilon*cot(phi_prev));
phi = atan(u*(1+a)/(omega*r*(1 - a_t)));

while (abs(a_prev - a) > 1e-2 || abs(a_t_prev - a_t) > 1e-2 || abs(phi_prev-phi) >1e-2)
a_prev = a;
a_t_prev = a_t;
phi_prev = phi;

alpha = (twist+pitch) - phi_prev;
[cl,cd,~] = polars(alpha,0.2);
epsilon = cd/cl;
% cl = 0.8;
% cd = 0.01;

a = B*cl*c*(cos(phi_prev)-epsilon*sin(phi_prev))/(8*r*pi*(sin(phi_prev))^2 +B*cl*c*epsilon*sin(phi_prev)-B*cl*c*cos(phi_prev));
a_t = B*cl*c*(epsilon*cot(phi_prev)+1)/(B*cl*c+8*pi*r*cos(phi_prev)+B*cl*c*epsilon*cot(phi_prev));
phi = atan(u*(1+a)/(omega*r*(1 - a_t)));
end
phi_end = phi;
a_end = a;
a_t_end = a_t;
alpha_end = alpha;
end 