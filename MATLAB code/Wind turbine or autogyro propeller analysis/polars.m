function [C_L,C_D,C_M] = polars(alpha)
% this function returns the lift, drag, and moment coefficients given the
% angle of attack alpha in rad

aoa = rad2deg(alpha);

if (aoa >= 180)
    aoa =  aoa - 360;  
end

aero = load('CL_CD_CM_PROP.mat');

a = aero.clcdcmpropeller.aoa_deg; % angle of attack in degrees
cl = aero.clcdcmpropeller.CL_M_02; % Cl at M = 0.2
cd = aero.clcdcmpropeller.CD_M_02; % Cd at M = 0.2
cm = aero.clcdcmpropeller.CM_M_02; % Cm at M = 0.2

        C_L = interp1(a ,cl,aoa);
        C_D = interp1(a ,cd,aoa);
        C_M = interp1(a ,cm,aoa);
end 



