The scope of this project is the analysis and evaluation of a propeller performance.
The Blade Element Momentum theory is used. Thus, the performance of the pro-
peller in terms of thrust and power coefficients, along with the propeller efficiency,
is calculated. Moreover, the impact of the pitch, the free stream and rotational
velocities on a given rotor’s performance is being investigated. The simulation is
performed with the use of MATLAB.
