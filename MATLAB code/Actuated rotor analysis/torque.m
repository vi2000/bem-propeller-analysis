function [Q, A,Ind_factors,Tan_ind_factors] = torque(u,omega,d,B,pitch,geometry)
%% This function gives the cumulative torque of the propeller given the 
% u - velocity
% omega -the spin rate of the rotor - omega
% d - air density
% B - number of blades 
% geometry - interpolated radius, twist and chord - geometry(removed points near rotation center to solve non-convergence errors and negative induction factors)
%%

r = geometry(:,1);
c = geometry(:,2);
t = geometry(:,3);
Ind_factors = zeros(1, length(r) - 1);
Tan_ind_factors = zeros(1, length(r) - 1);
Q_differential = zeros(length(r)-1,1);
A = zeros(1, length(r) - 1);
for i = 2:length(r)    
[a, a_t,alpha]= induction_factor(u,omega,r(i),B,c(i),t(i),pitch);
Q_differential(i-1) = 4*pi*r(i)^3*d*u*omega*(1+a)*a_t*(r(i) - r(i-1));
A(i) = alpha;
Ind_factors(i) = a;
Tan_ind_factors(i) = a_t;
end
Q = sum(Q_differential);


end