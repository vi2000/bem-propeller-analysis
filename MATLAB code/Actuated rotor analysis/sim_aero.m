%% BEM simulation of a rotor performance.

%% Define Geometry

chordtwistpropeller = [0	0	0
0.160000000000000	0.150000000000000	25.9200000000000
0.200000000000000	0.150000000000000	25.9200000000000
0.240000000000000	0.151600000000000	25.9200000000000
0.280000000000000	0.160000000000000	25.9200000000000
0.330300000000000	0.181000000000000	25.1500000000000
0.380500000000000	0.199100000000000	23.6200000000000
0.430800000000000	0.213100000000000	21.8400000000000
0.481000000000000	0.223000000000000	19.8700000000000
0.555000000000000	0.234300000000000	16.7100000000000
0.629000000000000	0.245300000000000	13.7700000000000
0.703000000000000	0.255400000000000	11.1200000000000
0.777000000000000	0.264700000000000	8.28000000000000
0.851000000000000	0.270400000000000	6.28000000000000
0.925000000000000	0.275500000000000	4.07000000000000
0.999000000000000	0.278100000000000	2.09000000000000
1.05450000000000	0.276700000000000	0.760000000000000
1.11000000000000	0.268600000000000	-0.537400000000000
1.16550000000000	0.254200000000000	-1.39400000000000
1.27640000000000	0.206400000000000	-2.40560000000000
1.38740000000000	0.138400000000000	-2.47400000000000
1.48000000000000	0.0469000000000000	-2.47400000000000];
r_interpol = linspace(0.308,1.480,100);
chord = chordtwistpropeller(:,2);
twist = chordtwistpropeller(:,3);
r = chordtwistpropeller(:,1);
c = interp1(r ,chord,r_interpol);
t = interp1(r ,twist,r_interpol);
geometry = [r_interpol', c', t'];

%% Model Parameters

B = 4; %number of blades 
rho = 1.184;%dry air density,25° C,sea level
u = 343*0.2; %m/s
D = 1.48*2; %% max r -> Rotor diameter


%% Simulate 
% You need to see where the simulation stops to converge,
% the higher the pitch, the higher the J, or the rpm in this case 

 %% For 30 deg pitch 
% pitch = 30;
% range = 950:50:1700;
% 
% CT1= zeros(1,length(range));
% CP1 = zeros(1,length(range));
% J1 = zeros(1,length(range));
% eta_p1 = zeros(1,length(range));
% 
% 
% i = 1;
% 
% for rpm=range
% omega = rpm/9.5492965964254;    
% n = omega*0.1592 ; % Hz
% T = thrust(u,omega,rho,B,pitch,geometry);
% Q = torque(u,omega,rho,B,pitch,geometry);
% CT1(1,i) = T/(rho*n^2*D^4);
% CP1(1,i) = Q*omega/(rho*n^3*D^5);
% J1(1,i) = u/(D*n);
% eta_p1(1,i) = T/Q*u/omega;
% i = 1+i;
% end
% pitch30 = [J1;CT1;CP1;eta_p1]';

 %% For 35 deg pitch 
% pitch = 35;
% range = 785:50:1830;
% CT4= zeros(1,length(range));
% CP4 = zeros(1,length(range));
% J4 = zeros(1,length(range));
% eta_p4 = zeros(1,length(range));
% 
% 
% i = 1;
% 
% for rpm=range
% omega = rpm/9.5492965964254;    
% n = omega*0.1592 ; % Hz
% T = thrust(u,omega,rho,B,pitch,geometry);
% Q = torque(u,omega,rho,B,pitch,geometry);
% CT4(1,i) = T/(rho*n^2*D^4);
% CP4(1,i) = Q*omega/(rho*n^3*D^5);
% J4(1,i) = u/(D*n);
% eta_p4(1,i) = T/Q*u/omega;
% i = 1+i;
% end
% 
% pitch35 = [J4;CT4;CP4;eta_p4]';

%% For 40 deg pitch 
pitch = 40;
range = 900:50:2000;

CT2= zeros(1,length(range));
CP2 = zeros(1,length(range));
J2 = zeros(1,length(range));
eta_p2 = zeros(1,length(range));


rpm = 1500;
omega = rpm/9.5492965964254;    
[T_no_loss,~] = thrust(u,omega,rho,B,pitch,geometry,0);
[T_loss,F_loss,r] = thrust(u,omega,rho,B,pitch,geometry,1);

i = 1;

%  for rpm=range
% omega = rpm/9.5492965964254;    
% n = omega*0.1592 ; % Hz
% T = thrust(u,omega,rho,B,pitch,geometry,0);
% %Q = torque(u,omega,rho,B,pitch,geometry);
% CT2(1,i) = T/(rho*n^2*D^4)
% %CP2(1,i) = Q*omega/(rho*n^3*D^5);
% J2(1,i) = u/(D*n);
% %eta_p2(1,i) = T/Q*u/omega;
% disp('40 deg')
% i = 1+i;
%  end
%pitch40 = [J2;CT2;CP2;eta_p2]';
% pitch40 = [J2;CT2]';
%% For 45 deg pitch 
% pitch = 45;
% range = 550:50:2500;
% 
% CT5= zeros(1,length(range));
% CP5 = zeros(1,length(range));
% J5 = zeros(1,length(range));
% eta_p5 = zeros(1,length(range));
% 
% 
% i = 1;
% 
% for rpm=range
% omega = rpm/9.5492965964254;    
% n = omega*0.1592 ; % Hz
% T = thrust(u,omega,rho,B,pitch,geometry);
% Q = torque(u,omega,rho,B,pitch,geometry);
% CT5(1,i) = T/(rho*n^2*D^4);
% CP5(1,i) = Q*omega/(rho*n^3*D^5);
% J5(1,i) = u/(D*n);
% eta_p5(1,i) = T/Q*u/omega;
% disp('45 deg')
% i = 1+i;
% end
% pitch45 = [J5;CT5;CP5;eta_p5]';

%% For 50 deg pitch 
% pitch = 50;
% range = 460:50:2600;
% 
% CT3= zeros(1,length(range));
% CP3 = zeros(1,length(range));
% J3 = zeros(1,length(range));
% eta_p3 = zeros(1,length(range));
% 
% 
% i = 1;
% 
% for rpm=range
% omega = rpm/9.5492965964254;    
% n = omega*0.1592 ; % Hz
% T = thrust(u,omega,rho,B,pitch,geometry);
% Q = torque(u,omega,rho,B,pitch,geometry);
% CT3(1,i) = T/(rho*n^2*D^4);
% CP3(1,i) = Q*omega/(rho*n^3*D^5);
% J3(1,i) = u/(D*n);
% eta_p3(1,i) = T/Q*u/omega
% disp('50 deg')
% i = 1+i;
% end
% pitch50 = [J3;CT3;CP3;eta_p3]';
% 
% 
% 
