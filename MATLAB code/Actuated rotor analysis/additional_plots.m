%% Define Geometry

chordtwistpropeller = [0	0	0
0.160000000000000	0.150000000000000	25.9200000000000
0.200000000000000	0.150000000000000	25.9200000000000
0.240000000000000	0.151600000000000	25.9200000000000
0.280000000000000	0.160000000000000	25.9200000000000
0.330300000000000	0.181000000000000	25.1500000000000
0.380500000000000	0.199100000000000	23.6200000000000
0.430800000000000	0.213100000000000	21.8400000000000
0.481000000000000	0.223000000000000	19.8700000000000
0.555000000000000	0.234300000000000	16.7100000000000
0.629000000000000	0.245300000000000	13.7700000000000
0.703000000000000	0.255400000000000	11.1200000000000
0.777000000000000	0.264700000000000	8.28000000000000
0.851000000000000	0.270400000000000	6.28000000000000
0.925000000000000	0.275500000000000	4.07000000000000
0.999000000000000	0.278100000000000	2.09000000000000
1.05450000000000	0.276700000000000	0.760000000000000
1.11000000000000	0.268600000000000	-0.537400000000000
1.16550000000000	0.254200000000000	-1.39400000000000
1.27640000000000	0.206400000000000	-2.40560000000000
1.38740000000000	0.138400000000000	-2.47400000000000
1.48000000000000	0.0469000000000000	-2.47400000000000];

r_interpol = linspace(0.308,1.480,100);
chord = chordtwistpropeller(:,2);
twist = chordtwistpropeller(:,3);
r = chordtwistpropeller(:,1);
c = interp1(r ,chord,r_interpol);
t = interp1(r ,twist,r_interpol);
geometry = [r_interpol', c', t'];
r = r_interpol';

%% Model Parameters

B = 4; %number of blades 
rho = 1.184;%dry air density,25° C,sea level
u = 343*0.2; %m/s
D = 1.48*2; %% max r -> Rotor diameter


%% Simulate 
% You need to see where the simulation stops to converge,
% the higher the pitch, the higher the J, or the rpm in this case 

%% For 30 deg pitch 
pitch = 40;


rpm = 2500;
omega = rpm/9.5492965964254;    
n = omega*0.1592 ; % Hz
[~, A1,Ind_factors1,Tan_ind_factors1] = torque(u,omega,rho,B,pitch,geometry);
J1 = u/(D*n);



rpm = 600;
omega = rpm/9.5492965964254;    
n = omega*0.1592 ; % Hz
T = thrust(u,omega,rho,B,pitch,geometry);
[~, A2,Ind_factors2,Tan_ind_factors2] = torque(u,omega,rho,B,pitch,geometry);
J2 = u/(D*n);
A2 = rad2deg(A2);

% 
figure(1)
t = tiledlayout(1,2);
title(t,"Angle of Attack")

nexttile
plot(r', A1, linewidth = 1.5)
xlabel("r(m)")
ylabel("α(deg)")

legend('J = 0.5561')
grid on

nexttile
plot(r', A2, linewidth = 1.5)
xlabel("r(m)")
ylabel("α(deg)")
legend('J = 2.3169')
grid on


% 
figure(2)
t = tiledlayout(1,2);
title(t,"Induction Factor")

nexttile
plot(r', Ind_factors1, linewidth = 1.5)
xlabel("r(m)")
ylabel("a")

legend('J = 0.5561')
grid on

nexttile
plot(r', Ind_factors2, linewidth = 1.5)
xlabel("r(m)")
ylabel("a")
legend('J = 2.3169')
grid on


% 
figure(3)
t = tiledlayout(1,2);
title(t,"Tangential Induction Factor")

nexttile
plot(r', Tan_ind_factors1, linewidth = 1.5)
xlabel("r(m)")
ylabel("a'")

legend('J = 0.5561')
grid on

nexttile
plot(r', Tan_ind_factors2, linewidth = 1.5)
xlabel("r(m)")
ylabel("a'")
legend('J = 2.3169')
grid on

