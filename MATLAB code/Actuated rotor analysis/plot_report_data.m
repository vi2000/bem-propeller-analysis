%% Plot data
 
% load('simulated_data/pitch30.mat')
% J1 = pitch30(:,1);
% CT1= pitch30(:,2);
% CP1 = pitch30(:,3);
% eta_p1 = pitch30(:,4);
% 
% load('simulated_data/pitch35.mat')
% J2 = pitch35(:,1);
% CT2= pitch35(:,2);
% CP2 = pitch35(:,3);
% eta_p2 = pitch35(:,4);
% 
% load('simulated_data/pitch40.mat')
% J3 = pitch40(:,1);
% CT3= pitch40(:,2);
% CP3 = pitch40(:,3);
% eta_p3 = pitch40(:,4);
% 
% load('simulated_data/pitch45.mat')
% J4 = pitch45(:,1);
% CT4= pitch45(:,2);
% CP4 = pitch45(:,3);
% eta_p4 = pitch45(:,4);
% 
% load('simulated_data/pitch50.mat')
% J5 = pitch50(:,1);
% CT5= pitch50(:,2);
% CP5 = pitch50(:,3);
% load('simulated_data/pitch50.mat')
% J5 = pitch50(:,1);

% eta_p5 = pitch50(:,4);

% J = pitch40(:,1);
% CT_tip_loss= pitch40_tip_loss(:,2);
% CT_no_tip_loss= pitch40(:,2);

% Plot efficiency coefficient 
%  figure(1)
%  plot(J1,eta_p1,'b-*')
%  hold on
%  plot(J2,eta_p2,'r-*')
%  plot(J3,eta_p3,'k-*')
%  plot(J4,eta_p4,'g-*')
%  plot(J5,eta_p5,'m-*')
%  legend("Pitch: 30 deg","Pitch: 35 deg","Pitch: 40 deg","Pitch: 45 deg","Pitch: 50 deg")
%  xlabel("J")
%  ylabel("η_p")
%  title("Propeller efficiency")
%  hold off

 % Plot thrust coefficient 
%  figure(1)
%  plot(J,CT_no_tip_loss,'b-*')
%  hold on
%  plot(J,CT_tip_loss,'r-*')
%  legend("No Loss Model","Prandtl Tip Loss Model")
%  xlabel("J")
%  ylabel("C_T")
%  title("Thrust coefficient (pitch:40 deg)")
%  hold off

  % Plot tip loss factor
 figure(1)
 plot(r/r(end),F_loss')
 legend("Prandtl Tip Loss Model")
 ylabel("Tip-Loss Factor F")
 xlabel("r/R")
 txt = ['rpm: ' num2str(rpm)];
 text(0.4,0.7,txt)
 txt = ['inflow velocity: ' num2str(u) ' m/s'];
 text(0.4,0.6,txt)
 txt = {'pitch: 40 deg'};
 text(0.4,0.5,txt)
 txt = ['Cumulative Thrust with tip losses: ' num2str(T_loss) ' N'];
 text(0.4,0.4,txt)
 txt = ['Cumulative Thrust without tip losses: ' num2str(T_no_loss) ' N'];
 text(0.4,0.3,txt)
 hold off
 ylim([0,1.1])
 
 % Plot power coefficient 
%  figure(3)
%  plot(J1,CP1,'b-*')
%  hold on
%  plot(J2,CP2,'r-*')
%  plot(J3,CP3,'k-*')
%  plot(J4,CP4,'g-*')
%  plot(J5,CP5,'m-*')
%  legend("Pitch: 30 deg","Pitch: 35 deg","Pitch: 40 deg","Pitch: 45 deg","Pitch: 50 deg")
%  xlabel("J")
%  ylabel("C_T")
%  title("Power coefficient")
%  hold off



%% To do :
% 1. Plot J as a function of hta 
% 2. Plot C_T as a function of J
% 3. Plot C_P as a function of J
