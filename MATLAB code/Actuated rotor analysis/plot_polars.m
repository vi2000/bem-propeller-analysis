load('CL_CD_CM_PROP.mat')

a = clcdcmpropeller.aoa_deg;
cl = clcdcmpropeller.CL_M_02;
cd = clcdcmpropeller.CD_M_02;

% Create plots
t = tiledlayout(1,2);
title(t,"Airfoil Polars")

nexttile
plot(a, cl, linewidth = 1.5)
title("Lift Coefficient")
xlabel("α")
ylabel("C_L")
xlim([-180,180])
grid on

nexttile
plot(a,cd, linewidth = 1.5)
xlabel("α")
ylabel("C_D")
title("Drag Coefficient")
xlim([-180,180])
grid on
