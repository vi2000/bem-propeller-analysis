function [C_L,C_D,C_M] = polars(alpha,mach)
% this function returns the lift, drag, and moment coefficients given the
% angle of attack alpha in rad
MAX_MIN = 0;
MAX_MAX = 0.2;
aoa = rad2deg(alpha);

% while (aoa >= 180)
%     aoa =  aoa - 360;  
% end

aero = load('CL_CD_CM_PROP.mat');

a = aero.clcdcmpropeller.aoa_deg; % angle of attack in degrees
cl_00 = aero.clcdcmpropeller.CL_M_00; % Cl at M = 0.0
cd_00 = aero.clcdcmpropeller.CD_M_00; % Cd at M = 0.0
cm_00 = aero.clcdcmpropeller.CM_M_00; % Cm at M = 0.0
cl_02 = aero.clcdcmpropeller.CL_M_02; % Cl at M = 0.2
cd_02 = aero.clcdcmpropeller.CD_M_02; % Cd at M = 0.2
cm_02 = aero.clcdcmpropeller.CM_M_02; % Cm at M = 0.2

        cl_00_single = interp1(a ,cl_00,aoa);
        cl_02_single = interp1(a ,cl_02,aoa);
        cd_00_single = interp1(a ,cd_00,aoa);
        cd_02_single = interp1(a ,cd_02,aoa);
        cm_00_single = interp1(a ,cm_00,aoa);
        cm_02_single = interp1(a ,cm_02,aoa);


coefficients = polyfit([MAX_MIN, MAX_MAX], [cl_00_single, cl_02_single], 1);
a = coefficients (1);
b = coefficients (2);
C_L = a*mach+b;
coefficients = polyfit([MAX_MIN, MAX_MAX], [cd_00_single, cd_02_single], 1);
a = coefficients (1);
b = coefficients (2);
C_D = a*mach+b;
coefficients = polyfit([MAX_MIN, MAX_MAX], [cm_00_single, cm_02_single], 1);
a = coefficients (1);
b = coefficients (2);
C_M = a*mach+b;


end 



