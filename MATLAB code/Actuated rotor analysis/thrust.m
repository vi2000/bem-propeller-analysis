function [T,F,r]= thrust(u,omega,d,B,pitch, geometry,tip_loss_factor_use)
%% To simulate run sim_plots.m
%% This function gives the cumulative thrust of the propeller given the
% u - velocity
% omega -the spin rate of the rotor - omega
% d - air density
% B - number of blades 
% geometry - interpolated radius, twist and chord - geometry(removed points near rotation center to solve non-convergence errors and negative induction factors)
%%
R = geometry(end,1);
r = geometry(:,1);
c = geometry(:,2);
t = geometry(:,3);
T_differential = zeros(length(r)-1,1);
F = zeros(1,length(r));
% for a specific U, omega
for i = 2:length(r)    
[a,~, ~, phi] = induction_factor(u,omega,r(i),B,c(i),t(i),pitch);
T_differential(i-1) = 4*pi*r(i)*d*u^2*(1+a)*a*(r(i) - r(i-1));
if (tip_loss_factor_use == 1)
    f = B/2 *(R-r(i))/(r(i)*sin(phi));
    F(i) = 2/pi*acos(exp(-f));   
    T_differential(i-1) = T_differential(i-1)*F(i);
end
%fprintf('iter %2i',i);
%fprintf('\n');
end

T = sum(T_differential);

end
