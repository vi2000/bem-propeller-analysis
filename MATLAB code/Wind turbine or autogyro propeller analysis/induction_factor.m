function [a_end,a_t_end] = induction_factor(u,omega,r,B,c,t)
%% To simulate run sim_plots.m

%% This function gives the axial induction factor - a_end and the tangential induction factor - a_t_end

% Input:
% u: descent velocity 
% omega: spin rate of the rotor 
% c: chord
% t: twist
% B - number of blades

%%
%lamda_r = omega*r/u; % local tip-speed ratio
sigma_t = B*c/(r*2*pi); % local solidity
twist = deg2rad(t);
%Initialization


a_prev = 0.1; %axial induction factor
a_t_prev =0.01; % tangential induction factor
% a_prev = 0.25*(2+pi*lamda_r*sigma_t-sqrt(4-4*pi*lamda_r*sigma_t+pi*lamda_r^2*sigma_t*(8*t+pi*sigma_t))); %axial induction factor
% a_t_prev =0; % tangential induction factor
phi_prev = atan(u*(1-a_prev)/(omega*r*(1 + a_t_prev))); % advance angle

alpha = phi_prev - (twist+30);

[cl,cd,~] = polars(alpha);

   
a = 1/(1+4*(sin(phi_prev))^2/(sigma_t*(cl*cos(phi_prev)+cd*sin(phi_prev))));
a_t = 1/(-1+4*sin(phi_prev)*cos(phi_prev)/(sigma_t*(cl*sin(phi_prev)-cd*cos(phi_prev))));
phi = atan(u*(1-a)/(omega*r*(1 + a_t)));

while (abs(a_prev - a) > 1e-6 || abs(a_t_prev - a_t) > 1e-6 || abs(phi_prev-phi) >1e-6)
a_prev = a;
a_t_prev = a_t;
phi_prev = phi;

alpha = phi_prev - (twist+30);
[cl,cd,~] = polars(alpha);
% cl = 0.8;
% cd = 0.01;
a = 1/(1+4*(sin(phi_prev))^2/(sigma_t*(cl*cos(phi_prev)+cd*sin(phi_prev))));
a_t = 1/(-1+4*sin(phi_prev)*cos(phi_prev)/(sigma_t*(cl*sin(phi_prev)-cd*cos(phi_prev))));
phi = atan(u*(1-a)/(omega*r*(1 + a_t)));
end

a_end = a;
a_t_end = a_t;
end 