function x = newton_ralphson(f,fd,x0,n)

%% Variables explanation
% This algorithm solves the equation f(x) = o
% f: function f, which root I want to find
% fd: derivative of function f
% x0:initial value
% n: number of iterations

x = x0;

for i =1:n
    x = x - f(x)/fd(x);
end

end

%% Example of calling the function
% f = @(x) x^3-5;
% fd =@(x) 3*x^2;
% x0 = 1;
% n = 10
% newton_ralphson(f,fd,x0,n)