%% BEM simulation of a rotor performance.

clc;
clear;
% Model Parameters
B = 4; %number of blades 
rho = 1.184;%dry air density,25° C,sea level
u = 343*0.2; %m/s
omega = 500/9.5492965964254; %1rad/s = 9.5492965964254rpm
D = 1.48*2; %% max r -> Rotor diameter
n = omega*0.1592 ; % Hz
T = thrust(u,omega,rho,B);
Q = torque(u,omega,rho,B);


C_T = T/(rho*n^2*D^4)
C_P = Q/(rho*n^3*D^5)
J = u/(D*n)
eta_p = T/Q*u


%% To do :
% 1. Plot J as a function of n 
% 2. Find T and Q for every n
% 3. Plot C_T as a funtion of J, which is a function of n
% 4. Plot C_P as a funtion of J, which is a function of n

%% Questions
% 1. Ταχύτητα? u = 343*0.2; %m/s?? gia 0.2 mach
% 2. Τι ειναι το pitch στα διαγραμματα? Swashplate, collective pitch? ti na kanw me auto?
% 3. Συναρτησει του τι ειναι τα διαγραμματα σελ 26-28? ΚΑι τι πρεπει να πλοτταρω εγω απο αυτα?
% 4. Το πρωτο διγραμμα αντιστοιχει στο η_p και ΟΧΙ στην συχνοτητα?
% 5. Τι στροφες να παρω? Γενικα στο J τι μεταβλητη πρεπει να αλλαζει?
% 6. Το CP μου βγαινει πολυ μικρο, το eta_p δεν ειναι κατω του 1

